package mx.itesm.error_2070;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

/**
 * Created by me.
 *
 */

public class Personaje extends Objeto{

    private Animation<TextureRegion> spriteAnimado;         // Animación caminando
    private float timerAnimacion;                           // Tiempo para cambiar frames de la animación

    protected EstadoJuego juego = EstadoJuego.JUGANDO;
    protected EstadoProteccion estadoProteccion = EstadoProteccion.NO_PROTEGIDO;
    private EstadoDanio estado;
    private float tiempoDaniado;

    protected EstadoSalto estadoSalto = EstadoSalto.EN_PISO;

    private int turnDanio;
    private int permisoDanio;

    // Recibe una imagen con varios frames (ver marioSprite.png)
    public Personaje(Texture textura, float x, float y){
        // Lee la textura como región
        TextureRegion texturaCompleta = new TextureRegion(textura);

        TextureRegion[][] texturaPersonaje = texturaCompleta.split(121,211);

        spriteAnimado = new Animation(0.08f, texturaPersonaje[0][0], texturaPersonaje[0][1], texturaPersonaje[0][2],
                texturaPersonaje[0][3],texturaPersonaje[0][4],texturaPersonaje[0][5]);
        // Animación infinita
        spriteAnimado.setPlayMode(Animation.PlayMode.LOOP);
        // Inicia el timer que contará tiempo para saber qué frame se dibuja
        timerAnimacion = 0;
        // Crea el sprite con el personaje quieto (idle)
        sprite = new Sprite(texturaPersonaje[0][3]);    // PAUSA

        sprite.setPosition(x,y);    // Posición inicial
        tiempoDaniado = 0;
        estado = EstadoDanio.SIN_DANIO;
        turnDanio = 0;
        permisoDanio = 0;
    }

    // Dibuja el personaje
    public void dibujar(SpriteBatch batch, EstadoJuego estadoJuego){
        // Dibuja el personaje dependiendo del estadoMovimiento
        juego = estadoJuego;

        switch (juego){
            case JUGANDO:
                turnDanio +=1;
                if(turnDanio%12 == 0){
                    permisoDanio += 1;
                }

                timerAnimacion += Gdx.graphics.getDeltaTime();
                // Frame que se dibujará
                tiempoDaniado += Gdx.graphics.getDeltaTime();
                TextureRegion region = spriteAnimado.getKeyFrame(timerAnimacion);

                if(estado == EstadoDanio.DANIADO && permisoDanio%2==0){
                    batch.setColor(Color.RED);
                }

                batch.draw(region,sprite.getX(),sprite.getY());
                if(estado == EstadoDanio.DANIADO ){
                    batch.setColor(Color.WHITE);
                }

                break;

            case PAUSADO:
                sprite.draw(batch); // Dibuja el sprite estático
                break;
        }
    }

    public void setEstadoJuego(EstadoJuego state){juego = state;}


    public void setEstadoProteccion(EstadoProteccion estadoProteccion){this.estadoProteccion = estadoProteccion;}

    public EstadoProteccion getEstadoProteccion() {return estadoProteccion;}

    public void daniar(){
        if(estado!=EstadoDanio.DANIADO){
            estado=EstadoDanio.DANIADO;
            tiempoDaniado=0;
            permisoDanio = 0;
        }
    }

    public void danio(){
        switch (estado){
            case SIN_DANIO:
                turnDanio = 0;
                sprite.setColor(Color.WHITE);
                break;
            case DANIADO:
                sprite.setColor(Color.RED);

                if(tiempoDaniado>=2){
                    estado=EstadoDanio.SIN_DANIO;
                    tiempoDaniado=0;
                }
                break;
        }
    }

    public enum EstadoSalto{
        EN_PISO,
        SALTANDO    // General, puede estar subiendo o bajando
    }

    public enum EstadoProteccion{
        PROTEGIDO,
        NO_PROTEGIDO
    }

    public enum EstadoDanio{
        SIN_DANIO,
        DANIADO
    }

}
