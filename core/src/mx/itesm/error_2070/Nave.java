package mx.itesm.error_2070;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.TimeUtils;

/**
 * Created by Daniel Alillo on 01/11/2017.
 */

public class Nave extends Objeto{

    private final float VELOCIDAD_X = -400;      // Velocidad horizontal (a la izquierda)
    private float vida =5;
    private Animation<TextureRegion> spriteAnimado;         // Animación volando
    private float timerAnimacion;                           // Tiempo para cambiar frames de la animación

    private float position;
    private EstadoShoot estadoShoot;

    private EstadoJuego estadoJuego;
    private Animation<TextureRegion> explode;

    public Life life;
    public boolean muerto;
    public float timeToDie;

    public Nave(Texture textura, Texture explosion, float x, float y){
        // Lee la textura como región
        TextureRegion texturaCompleta = new TextureRegion(textura);
        // La divide en 2 frames de 32x64 (ver enemigo.png)
        TextureRegion[][] texturaPersonaje = texturaCompleta.split(249,131);
        // Crea la animación con tiempo de 0.25 segundos entre frames.
        spriteAnimado = new Animation(0.5f, texturaPersonaje[0][0], texturaPersonaje[0][1]);
        // Animación infinita
        spriteAnimado.setPlayMode(Animation.PlayMode.LOOP);
        // Inicia el timer que contará tiempo para saber qué frame se dibuja
        timerAnimacion = 0;

        TextureRegion texturaEx = new TextureRegion(explosion);
        TextureRegion[][] texturaExplota = texturaEx.split(182,153);
        explode =  new Animation(0.02f, texturaExplota[0][0], texturaExplota[0][1],
                texturaExplota[0][2],texturaExplota[0][3],texturaExplota[0][4],texturaExplota[0][5]
                ,texturaExplota[0][6],texturaExplota[0][7],texturaExplota[0][8],texturaExplota[0][9]
                ,texturaExplota[0][10],texturaExplota[0][11]);
        explode.setPlayMode(Animation.PlayMode.LOOP);

        life = Life.ALIVE;
        muerto = false;
        timeToDie = 0;

        // Crea el sprite
        sprite = new Sprite(texturaPersonaje[0][0]);
        sprite.setPosition(x,y);    // Posición inicial

        position= 1280;
        estadoShoot = EstadoShoot.PASSIVE;
        estadoJuego = EstadoJuego.JUGANDO;
        position = 1280;
    }

    // Dibuja la nave
    public void dibujar(SpriteBatch batch, EstadoJuego estadoJuego){
        if(life == Life.ALIVE){
            // Dibuja la nave dependiendo del estadoMovimiento
            if(estadoJuego == EstadoJuego.JUGANDO){
                if(sprite.getX()<position){
                    estadoShoot = EstadoShoot.AGGRESSIVE;
                    position = position -600;
                }
                timerAnimacion += Gdx.graphics.getDeltaTime();
                // Frame que se dibujará
                TextureRegion region = spriteAnimado.getKeyFrame(timerAnimacion);
                batch.draw(region, sprite.getX(), sprite.getY());
            }else{sprite.draw(batch);}
        }else{
            timeToDie += Gdx.graphics.getDeltaTime();
            //timeExplode += Gdx.graphics.getDeltaTime();
            TextureRegion region = explode.getKeyFrame(timeToDie);
            batch.draw(region, sprite.getX(), sprite.getY());
            if(timeToDie >0.25){muerto = true;}
        }
    }

    public void kill(){
        if(life == Life.ALIVE){life = Life.DEAD;}
    }


    public EstadoShoot getEstadoShoot(){return estadoShoot;}

    public void setEstadoShoot(EstadoShoot estadoShoot){this.estadoShoot = estadoShoot;}

    // Mueve el personaje a la izquierda
    public void mover(float delta){
        if (life == Life.ALIVE){
            float distancia = VELOCIDAD_X * delta;
            sprite.setX(sprite.getX() + distancia);
        }
    }

    public boolean clash(RobotRunner robotRunner){
        if(life == Life.ALIVE){
            return sprite.getBoundingRectangle().overlaps(robotRunner.sprite.getBoundingRectangle());
        }
        return false;
    }

    public float getVida(){return vida;}

    public void setVida(float menos){vida += menos;}

    public enum EstadoShoot{
        AGGRESSIVE,
        PASSIVE
    }

    public enum Life{
        DEAD,
        ALIVE
    }
}
